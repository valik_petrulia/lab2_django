from rest_framework.generics import (
	ListCreateAPIView,
	RetrieveUpdateDestroyAPIView
)
from .models import (
	Publisher,
	Writer,
	Artist,
	Genre,
	ComicBook
)
from .serializers import (
	PublisherSerializer,
	WriterSerializer,
	ArtistSerializer,
	GenreSerializer,
	ComicBookSerializer
)

class PublisherListAPIView(ListCreateAPIView):
    queryset = Publisher.objects.all()
    serializer_class = PublisherSerializer

class WriterListAPIView(ListCreateAPIView):
    queryset = Writer.objects.all()
    serializer_class = WriterSerializer

class ArtistListAPIView(ListCreateAPIView):
    queryset = Artist.objects.all()
    serializer_class = ArtistSerializer

class GenreListAPIView(ListCreateAPIView):
    queryset = Genre.objects.all()
    serializer_class = GenreSerializer

class ComicBookListAPIView(ListCreateAPIView):
    queryset = ComicBook.objects.all()
    serializer_class = ComicBookSerializer


class PublisherAPIView(RetrieveUpdateDestroyAPIView):
    queryset = Publisher.objects.all()
    serializer_class = PublisherSerializer

class WriterAPIView(RetrieveUpdateDestroyAPIView):
    queryset = Writer.objects.all()
    serializer_class = WriterSerializer

class ArtistAPIView(RetrieveUpdateDestroyAPIView):
    queryset = Artist.objects.all()
    serializer_class = ArtistSerializer

class GenreAPIView(RetrieveUpdateDestroyAPIView):
    queryset = Genre.objects.all()
    serializer_class = GenreSerializer

class ComicBookAPIView(RetrieveUpdateDestroyAPIView):
    queryset = ComicBook.objects.all()
    serializer_class = ComicBookSerializer