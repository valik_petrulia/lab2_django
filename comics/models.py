from django.db import models
import datetime


year_choices = []
for r in range(1938, (datetime.datetime.now().year + 1)):
    year_choices.append((r,r))


class Publisher(models.Model):
	name = models.CharField(max_length = 30, unique = True)
	address = models.CharField(max_length = 30)

	def __str__(self):
		return self.name


class Writer(models.Model):
	name = models.CharField(max_length = 30)
	surname = models.CharField(max_length = 30)

	def __str__(self):
		return self.name + " " + self.surname


class Artist(models.Model):
	name = models.CharField(max_length = 30)
	surname = models.CharField(max_length = 30)

	def __str__(self):
		return self.name + " " + self.surname


class Genre(models.Model):
	name = models.CharField(max_length = 30, unique = True)
	
	def __str__(self):
		return self.name


class ComicBook(models.Model):
	name = models.CharField(max_length = 50)
	year = models.IntegerField(choices = year_choices)
	publisher = models.ForeignKey(Publisher, on_delete = models.CASCADE)
	writer = models.ForeignKey(Writer, on_delete = models.CASCADE)
	artist = models.ForeignKey(Artist, on_delete = models.CASCADE)
	genres = models.ManyToManyField(Genre)

	def __str__(self):
		return self.name
 







