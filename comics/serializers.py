from rest_framework import serializers
from .models import (
	Publisher,
	Writer,
	Artist,
	Genre,
	ComicBook
)

class PublisherSerializer(serializers.ModelSerializer):
	class Meta:
		model = Publisher
		fields = ('id', 'name', 'address')


class WriterSerializer(serializers.ModelSerializer):
	class Meta:
		model = Writer
		fields = ('id', 'name', 'surname')


class ArtistSerializer(serializers.ModelSerializer):
	class Meta:
		model = Artist
		fields = ('id', 'name', 'surname')


class GenreSerializer(serializers.ModelSerializer):
	class Meta:
		model = Genre
		fields = ('id', 'name')


class ComicBookSerializer(serializers.ModelSerializer):
	class Meta:
		model = ComicBook
		fields = ('id', 'name', 'year', 'publisher', 'writer', 'artist', 'genres')
