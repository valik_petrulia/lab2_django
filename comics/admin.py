from django.contrib import admin
from .models import (
	Publisher,
	Writer,
	Artist,
	Genre,
	ComicBook
)

admin.site.register(Publisher)
admin.site.register(Writer)
admin.site.register(Artist)
admin.site.register(Genre)
admin.site.register(ComicBook)