from django.urls import path
from .views import (
	PublisherListAPIView,
	WriterListAPIView,
	ArtistListAPIView,
	GenreListAPIView,
	ComicBookListAPIView,
	PublisherAPIView,
	WriterAPIView,
	ArtistAPIView,
	GenreAPIView,
	ComicBookAPIView
)

app_name = 'comics'

urlpatterns = [
    path('publishers/', PublisherListAPIView.as_view()),
    path('publishers/<int:pk>/', PublisherAPIView.as_view()),
    path('writers/', WriterListAPIView.as_view()),
    path('writers/<int:pk>/', WriterAPIView.as_view()),
    path('artists/', ArtistListAPIView.as_view()),
    path('artists/<int:pk>/', ArtistAPIView.as_view()),
    path('genres/', GenreListAPIView.as_view()),
    path('genres/<int:pk>/', GenreAPIView.as_view()),
    path('comicbooks/', ComicBookListAPIView.as_view()),
    path('comicbooks/<int:pk>/', ComicBookAPIView.as_view()),
]